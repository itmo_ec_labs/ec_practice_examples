from pymoo.algorithms.so_genetic_algorithm import GA
from pymoo.optimize import minimize
from pymoo.model.problem import Problem
import random as rnd
from pymoo.interface import mutation, crossover
from pymoo.model.sampling import Sampling
from pymoo.model.crossover import Crossover
from pymoo.model.mutation import Mutation
from pymoo.model.individual import Individual
from pymoo.model.duplicate import DuplicateElimination
from pymoo.model.population import Population

import numpy as np


# problem
# factory
# mutation, crossover
# selection - selection from framework

class Item:
    def __init__(self, weight, value):
        self.weight = weight
        self.value = value


class KnapsackProblem(Problem):
    def __init__(self, constraint, items):
        super().__init__(1, n_obj=1, n_constr=0, type_var=set)
        self.constraint = constraint
        self.items = items

    def _evaluate(self, x, out, *args, **kwargs):
        constraint = self.constraint
        fit = []
        for inds in range(len(x)):
            result = 0.0
            weight = 0.0
            if hasattr(x[inds][0], "X"):
                backpack = list(x[inds][0].X[0])
            else:
                backpack = list(x[inds][0])
            for item in backpack:
                result += item.value
                weight += item.weight
            if weight > constraint:
                result = 0.0
            fit.append(result)
        out['F'] = np.array(fit).reshape((len(x), 1))


class KnapsackFactory(Sampling):
    def __init__(self, initial_size):
        self.initial_size = initial_size

    def _do(self, problem, n_samples, **kwargs):
        X = np.full((n_samples, 1), None, dtype=np.object)
        for n in range(n_samples):
            backpack = set()
            for j in range(self.initial_size):
                backpack.add(rnd.choice(problem.items))

            X[n, 0] = backpack
        return X


class KnapsackMutation(Mutation):
    def __init__(self):
        super().__init__()

    def do(self, problem, pop, **kwargs):
        for ind in pop:
            backpack = ind[0].X[0]
            size = len(backpack)
            if rnd.random() < 0.5:
                possible_items = [item for item in problem.items if item not in backpack]
                backpack.add(rnd.choice(possible_items))
            else:
                if size > 1:
                    backpack.pop()
        return pop


class KnapsackCrossover(Crossover):
    def __init__(self):
        # define the crossover: number of parents and number of offsprings
        super().__init__(2, 2)

    def do(self, problem, pop, parents, **kwargs):
        children = []
        for p1_idx, p2_idx in parents:
            c1, c2 = self.crossover(pop[p1_idx], pop[p2_idx])
            children.append(c1)
            children.append(c2)

        return np.array(children).reshape(len(children), 1)


    def crossover(self, b1, b2):
        c1 = b1.copy()
        c2 = b2.copy()
        c1.X[0] = b1.X[0] & b2.X[0]  # intersection of two sets
        c2.X[0] = b1.X[0] ^ b2.X[0]  # symmetric difference of theese two sets
        # if len(c1.X[0]) == 0:
        #     c1.X[0].add(b1.X[0].pop())
        # if len(c2.X[0]) == 0:
        #     c2.X[0].add(b2.X[0].pop())
        return c1, c2


class KnapsackDuplicateElimination(DuplicateElimination):

    def __init__(self, func=None) -> None:
        super().__init__(func)

    def do(self, pop, *args, return_indices=False, to_itself=True):
        return pop


if __name__ == "__main__":
    items = [Item(rnd.randint(1, 10), rnd.randint(1, 20)) for _ in range(50)]
    backpack_weight = 100

    problem = KnapsackProblem(backpack_weight, items)

    pop_size = 50
    mut = KnapsackMutation()
    cross = KnapsackCrossover()
    factory = KnapsackFactory(10)
    eliminator = KnapsackDuplicateElimination()

    algorithm = GA(pop_size, sampling=factory, mutation=mut, crossover=cross, eliminate_duplicates=eliminator)

    res = minimize(problem,
                   algorithm,
                   verbose=True)

    print(res)
